import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Splash from '../Splash';
import Home from '../Home';
import Signin from '../Signin';
import Create from '../Create';
const RootStack = createNativeStackNavigator();
const approuter = () => {
    return (
        <NavigationContainer>

            <RootStack.Navigator>
                <RootStack.Screen name="Splash" component={Splash} />
                <RootStack.Screen name="Home" component={Home} />
                <RootStack.Screen name="Login" component={Signin} />
                <RootStack.Screen name="Create" component={Create} />
                 

            </RootStack.Navigator>
            
        </NavigationContainer>
    )
}
export default approuter