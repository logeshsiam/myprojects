import React, { useEffect } from "react";
import { View, Image } from "react-native";


const Splash = ({ navigation }) => {
    useEffect(() => {
        setTimeout(() => {
            navigation.navigate('Home')
        }, 3000);
    }, [])

    return (
        <View>
            <Image style={{ height: 725 }} source={require('../assets/phone.jpg')} ></Image>
        </View>
    );
};
export default Splash;

