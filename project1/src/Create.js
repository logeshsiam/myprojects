import React from 'react';

import auth from '@react-native-firebase/auth';

import {View, Text,StyleSheet,Image,TouchableOpacity,SafeAreaView,TextInput, Button, Alert } from 'react-native'

import Icon from 'react-native-vector-icons/FontAwesome';

const Create = () => {


  const [NameText, setNameText] = React.useState('')
  const [EmailText, setEmailText] = React.useState('')
  const [PassText, setPassText] = React.useState('')

const validation = () => {

  if (NameText === '')
     {Alert.alert('Please Fill the Name')}
 else if (EmailText === '')
     {Alert.alert('Please Fill the Email')}
 else if (PassText === '')
      {Alert.alert('Please Fill the Password')}
else{Alert.alert('Success')}

}


auth()
  .createUserWithEmailAndPassword('logeshsiam@gmail.com', 'SuperSecretPassword!')
  .then(() => {
    console.log('User account created & signed in!');
  })
  .catch(error => {
    if (error.code === 'auth/email-already-in-use') {
      console.log('That email address is already in use!');
    }

    if (error.code === 'auth/invalid-email') {
      console.log('That email address is invalid!');
    }

    console.error(error);
  });

return (
<View style={{backgroundColor:"white",flex:1}}>
  
<View style={{marginTop:40}}></View>

<Text style={{ fontWeight:"bold",fontSize:40,textAlign:"justify",color:"#000080",marginLeft:30,direction:50,marginTop:40}}>HI !</Text>

<Text style={{fontSize:15,margintop:5,textAlign:"left",color:"#0000FF",marginLeft:30}}>Create a new account </Text>


<View style={{marginTop:30}}></View>


<TextInput placeholder= 'Name'
        style={{height: 40,
    margin: 12,
    borderBottomWidth: 0.5,
    padding: 10,
    marginLeft:75,
     width:250}} 

     onChangeText={(text) => setNameText(text) }

      />


<TextInput placeholder= 'Example@gmail.com'
        style={{height: 40,
    margin: 12,
    borderBottomWidth: 0.5,
    padding: 10,
    marginLeft:75,
     width:250}} keyboardType="email-address"

     onChangeText={(text) => setEmailText(text) }

      />



<TextInput placeholder='Password'
        style={{height: 40,
    margin: 12,
    borderBottomWidth: 0.5,
    padding: 10,
    width:250,
    marginLeft:75
  }}

  onChangeText={(text) => setPassText(text) }

      secureTextEntry={true}  />


<View style={{marginTop:50}}></View>

<View style={{alignItems:"center"}}  >
<TouchableOpacity onPress={validation}
        style={{
            margin:10,
            height:40,
            width:200,
            justifyContent: "center",
            paddingHorizontal: 10,
            backgroundColor: "#0000a5",
            padding: 10
          }}
      >
        <Text style={{textAlign:"center", color:"white"}} >SIGN UP</Text>
      </TouchableOpacity>
      </View>


      <View style={{marginTop:45}}></View>

      <Text style={{fontSize:13,textAlign:"center",color:"#00008B"}}>Social Media Login </Text>


      <View style={{marginTop:20}}></View>
      
      <View style={{height:30,width:30,marginLeft:180}} >
   <Text>
     <Icon name="facebook-square" size={30} color="#0000ad"/>;
   </Text>
 </View>
    

      <View style={{marginTop:30}}></View>

<Text style={{fontSize:13,textAlign:"center",color:"#00008B"}}>Do You Have A Account ?  <Text style={{fontSize:12,textAlign:"left",color:"#191970",fontWeight:"bold"}}  >Sign Up </Text> </Text>


  </View>


);
};

  export default Create;
  