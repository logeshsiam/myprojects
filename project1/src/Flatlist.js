import React, {Component} from 'react';  
import { StyleSheet, Text, View, SafeAreaView, FlatList, Image} from 'react-native';

const DATA = [
  {
    "discount": "10,000",
    "endsin": "Today",
    "name": "Mobile",
    "rate": "12,000",
    "url": "https://m.media-amazon.com/images/I/41apNNfINPL.__AC_SY200_.jpg"
  },
  {
    "discount": "150",
    "endsin": "Today",
    "name": "Watch",
    "rate": "214",
    "url": "https://m.media-amazon.com/images/I/41TvSQNosiL.__AC_SY200_.jpg"
  },
  {
    "discount": "250",
    "endsin": "Today",
    "name": "HandBag",
    "rate": "319",
    "url": "https://m.media-amazon.com/images/I/41x7KLp5JiL.__AC_SY200_.jpg"
  },
  {
    "discount": "1000",
    "endsin": "Today",
    "name": "Dress",
    "rate": "2000",
    "url": "https://m.media-amazon.com/images/I/51eibVsPieL.__AC_SY200_.jpg"
  },
  {
    "discount": "800",
    "endsin": "Today",
    "name": "Headset",
    "rate": "1500",
    "url": "https://m.media-amazon.com/images/I/41LodhV-waL.__AC_SY200_.jpg"
  },
  {
    "discount": "750",
    "endsin": "Today",
    "name": "Pant",
    "rate": "1200",
    "url": "https://m.media-amazon.com/images/I/41QFLArTr+L.__AC_SY200_.jpg"
  }
]


;


export default class Flatlist extends Component {
    render() {
        return(
            <SafeAreaView style={styles.container}>
                <FlatList
                data= {DATA}
                renderItem={({item}) =>(
                  
                    <View style={styles.item} style={{flexDirection:'row',backgroundColor:'white', padding:15,marginVertical:5,
                    marginHorizontal:20, justifyContent:'center',borderRadius:25,borderWidth:1, borderColor:'black'}} >
                       
                        <View style={{flex:1,}}>

                        <Image style={{ height:200, width:160,padding:50, resizeMode:'contain', }} source= {{uri: item.url}}/>

                        </View>
                       
                        <View style={{ flex:1,marginLeft:40}}>

                        <Text style={{fontSize:25, fontWeight:'700',color:'black',textAlign:'justify'}}>{item.name}</Text>

                        <Text style={{fontSize:22,color:'blue',fontWeight:'600',marginTop:20,textAlign:'justify'}}>₹ {item.rate} </Text>
                        
                        <Text style={{fontSize:15,marginTop:20,color:'green',fontWeight:'500',textAlign:'justify'}}>Discount ₹ {item.discount}</Text>


                        <Text style={{fontSize:15,fontWeight:'900',marginTop:20,color:'violet',textAlign:'justify'}}>Ends in {item.endsin}</Text>
                        
                        </View>
                        
                    </View>
                )}
               
                />
            </SafeAreaView>
        );
    }
}

const styles= StyleSheet.create({
    container:{
      flex:1,
       
    },
    item: {
      backgroundColor:'white',
      padding:20,
      marginVertical:10,
      marginHorizontal:16,
      justifyContent:'center',
      borderRadius:8,
      borderWidth:2,
      borderColor:'black',
     
        

    },

});
