
import React from 'react';

import {View, Text,StyleSheet,Image,TouchableOpacity, Button } from 'react-native'

const Home = ({navigation}) => {


return (
<View style={{backgroundColor:"white",flex:1}}>

<Image source={require('../assets/bg1.png')} style={{height:400,width:400}} />
<Text style={{fontWeight:"bold",fontSize:40,textAlign:"center",color:"black"}}>Hello ! </Text>

<Text style={{fontSize:10,textAlign:"center",color:"black"}}>Best place to write life stories and </Text>
<Text style={{fontSize:10,textAlign:"center",color:"black"}}>Share your journey experiences </Text>


<View style={{marginTop:20}}> 



<View style={{alignItems:"center"}}  >
<TouchableOpacity
        style={{
            margin:10,
            height:40,
            width:200,
            justifyContent: "center",
            paddingHorizontal: 10,
            backgroundColor: "#0000a5",
            padding: 10
          }}
          onPress={()=>navigation.navigate('Login')}
      >
        <Text style={{textAlign:"center", color:"white"}} >LOG IN</Text>
      </TouchableOpacity>
      </View>

  

<View style={{alignItems:"center"}} >

  <TouchableOpacity
        style={{
            margin:10,
            height:40,
            width:200,
            justifyContent: "center",
            paddingHorizontal: 10,
            borderColor:"#0000a5",
            borderWidth:2,
            backgroundColor:"white",
            padding: 10
          }}
          onPress={()=>navigation.navigate('Create')} >

       <Text style={{textAlign:"center", color:"#0000a5"}}>SIGN UP</Text>
       
  </TouchableOpacity>

</View>

      </View>

</View>
  


);
};

  export default Home;
