/**
 * @format
 */

import {AppRegistry} from 'react-native';

import {name as appName} from './app.json';
import approuter from './src/router/approuter'

AppRegistry.registerComponent(appName, () =>approuter);

